#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <dirent.h>
#define PORT 8080

int login(char *username, char *password);
char *sql(char q[1024], int auth);
int insert_into_users(char *username, char *password);
int add_db(char db_name[1024], char username[1024]);
int check_db(char db_name[1024], char username[1024]);
void add_user_conf(char path[1024], char username[1024]);
int grant_user(char db_name[1024], char username[1024]);

int main(int argc, char const *argv[])
{
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    // if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
    //     perror("accept");
    //     exit(EXIT_FAILURE);
    // }
    // pid_t pid, sid;        // Variabel untuk menyimpan PID

    // pid = fork();     // Menyimpan PID dari Child Process

    // /* Keluar saat fork gagal
    // * (nilai variabel pid < 0) */
    // if (pid < 0) {
    //     exit(EXIT_FAILURE);
    // }

    // /* Keluar saat fork berhasil
    // * (nilai variabel pid adalah PID dari child process) */
    // if (pid > 0) {
    //     exit(EXIT_SUCCESS);
    // }

    // umask(0);

    // sid = setsid();
    // if (sid < 0) {
    //     exit(EXIT_FAILURE);
    // }

    // if ((chdir("/home/kali/fp sisop/database")) < 0) {
    //     exit(EXIT_FAILURE);
    // }

    // close(STDIN_FILENO);
    // close(STDOUT_FILENO);
    // close(STDERR_FILENO);
    int logged_in = 0;
    char login_data[1024] = "";
    char username[1024] = "";
    char password[1024] = "";
    char msg[1024] = "";
    while (1)
    {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
        {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        valread = read(new_socket, login_data, 1024);
        // valread = read( new_socket , password, 1024);

        char *token = strtok(login_data, ":");
        strcpy(username, token);
        token = strtok(NULL, ":");
        strcpy(password, token);
        // printf("%s %s\n", username, password);
        memset(login_data, 0, 1024);
        logged_in = login(username, password);

        if (logged_in >= 1)
        {
            sprintf(msg, "login as %s\n", username);
            printf("%s", msg);
            send(new_socket, msg, strlen(msg), 0);
            char *return_sql;
            while (1)
            {
                /* code */
                valread = read(new_socket, buffer, 1024);
                // status = sql(buffer, logged_in);

                char q[1024];
                strcpy(q, buffer);
                memset(buffer, 0, 1024);
                char *ptr = strtok(q, " ");
                // char *query[10];
                // int i = 0;
                // printf("%s\n", ptr);
                if (strcmp(ptr, "CREATE") == 0)
                {
                    ptr = strtok(NULL, " ");
                    if (strcmp(ptr, "USER") == 0)
                    {
                        if (logged_in != 2)
                        {
                            // Not allowed
                            strcpy(msg, "Access Denied!");
                            send(new_socket, msg, strlen(msg), 0);
                        }
                        else
                        {
                            char *usr;
                            char *pass;

                            ptr = strtok(NULL, " ");
                            // printf("%s\n", ptr);
                            usr = ptr;
                            ptr = strtok(NULL, " ");
                            if (strcmp(ptr, "IDENTIFIED") != 0)
                            {
                                // Wrong query
                                strcpy(msg, "Wrong SQL Syntax!");
                                send(new_socket, msg, strlen(msg), 0);
                            }
                            else
                            {
                                ptr = strtok(NULL, " ");
                                if (strcmp(ptr, "BY") != 0)
                                {
                                    // Wrong query
                                    strcpy(msg, "Wrong SQL Syntax!");
                                    send(new_socket, msg, strlen(msg), 0);
                                }
                                else
                                {
                                    ptr = strtok(NULL, " ");
                                    pass = ptr;
                                    int status = insert_into_users(usr, pass);
                                    printf("%d\n", status);
                                    if (!status)
                                    {
                                        strcpy(msg, "Username Already Exist.");
                                        send(new_socket, msg, strlen(msg), 0);
                                    }
                                    else
                                    {
                                        strcpy(msg, "SQL Executed Successfully!");
                                        send(new_socket, msg, strlen(msg), 0);
                                    }
                                }
                            }
                        }
                    }
                    else if (strcmp(ptr, "DATABASE") == 0)
                    {
                        ptr = strtok(NULL, " ");
                        char db_name[1024];
                        strcpy(db_name, ptr);
                        int status = add_db(db_name, username);
                        if (!status)
                        {
                            strcpy(msg, "Database Already Exist!");
                            send(new_socket, msg, strlen(msg), 0);
                        }
                        else
                        {
                            strcpy(msg, "SQL Executed Successfully!");
                            send(new_socket, msg, strlen(msg), 0);
                        }
                    }
                    else if (strcmp(ptr, "TABLE") == 0)
                    {
                        strcpy(msg, "Must use USE [DATABASE] First!");
                        send(new_socket, msg, strlen(msg), 0);
                    }
                }
                else if (strcmp(ptr, "USE") == 0)
                {
                    ptr = strtok(NULL, " ");
                    char db_name[1024];
                    strcpy(db_name, ptr);
                    int status = check_db(db_name, username);
                    if (status == -1)
                    {
                        strcpy(msg, "Database not Exist!");
                        send(new_socket, msg, strlen(msg), 0);
                    }
                    else if (status == -2)
                    {
                        strcpy(msg, "Acces Denied: Invalid Username");
                        send(new_socket, msg, strlen(msg), 0);
                    }
                    else
                    {
                        sprintf(msg, "Database: %s", db_name);
                        send(new_socket, msg, strlen(msg), 0);
                        while (1)
                        {
                            char command[1024];
                            valread = read(new_socket, buffer, 1024);
                            strcpy(command, buffer);
                            memset(buffer, 0, sizeof(buffer));
                            char *cmd = strtok(command, " (),");
                            if (strcmp(cmd, "CREATE") == 0)
                            {
                                cmd = strtok(NULL, " (),");
                                if (strcmp(cmd, "TABLE") == 0)
                                {
                                    char table_name[1024], column[1024][1024], type[1024][1024];
                                    cmd = strtok(NULL, " (),");
                                    strcpy(table_name, cmd);
                                    int idx = 0;
                                    while (cmd != NULL)
                                    {
                                        cmd = strtok(NULL, " (),");
                                        strcpy(column[idx], cmd);
                                        cmd = strtok(NULL, " (),");
                                        strcpy(type[idx], cmd);
                                        idx++;
                                    }
                                    //
                                }
                            }
                            else if (strcmp(cmd, "DROP") == 0)
                            {
                                cmd = strtok(NULL, " (),");
                                if (strcmp(cmd, "DATABASE") == 0)
                                {
                                    char db[1024];
                                    cmd = strtok(NULL, " (),");
                                    strcpy(db, cmd);
                                    //
                                }
                                else if (strcmp(cmd, "TABLE") == 0)
                                {
                                }
                            }
                        }
                    }
                }
                else if (strcmp(ptr, "DROP") == 0)
                {
                    strcpy(msg, "Must use USE [DATABASE] First!");
                    send(new_socket, msg, strlen(msg), 0);
                }
                else if (strcmp(ptr, "GRANT") == 0)
                {
                    if (logged_in != 2)
                    {
                        strcpy(msg, "Access Denied!");
                        send(new_socket, msg, strlen(msg), 0);
                    }
                    else
                    {
                        ptr = strtok(NULL, " ");
                        if (strcmp(ptr, "PERMISSION") != 0)
                        {
                            strcpy(msg, "Wrong SQL Syntax!");
                            send(new_socket, msg, strlen(msg), 0);
                        }
                        else
                        {
                            char db_name[1024], usr[1024];
                            ptr = strtok(NULL, " ");

                            strcpy(db_name, ptr);
                            ptr = strtok(NULL, " ");
                            if (strcmp(ptr, "INTO") != 0)
                            {
                                strcpy(msg, "Wrong SQL Syntax!");
                                send(new_socket, msg, strlen(msg), 0);
                            }
                            else
                            {
                                ptr = strtok(NULL, " ");
                                strcpy(usr, ptr);
                                // printf("%s %s", db_name, usr);
                                // printf("%s\n",ptr);
                                int status = grant_user(db_name, usr);
                                if (!status)
                                {
                                    strcpy(msg, "Database doesn't Exist!");
                                    send(new_socket, msg, strlen(msg), 0);
                                }
                                else
                                {
                                    strcpy(msg, "SQL Executed Successfully!");
                                    send(new_socket, msg, strlen(msg), 0);
                                }
                            }
                        }
                    }
                }
                else
                {
                    strcpy(msg, "Invalid SQL Command");
                    send(new_socket, msg, strlen(msg), 0);
                }
            }
        }
        else
        {
            strcpy(msg, "wrong username/password!");
            send(new_socket, msg, strlen(msg), 0);
        }

        // send(new_socket , hello , strlen(hello) , 0 );
        // printf("Hello message sent\n");
    }
}

int login(char *username, char *password)
{
    if (strcmp(username, "root") == 0 && strcmp(password, "root") == 0)
    {
        return 2;
    }
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    if (fp == NULL)
    {
        exit(EXIT_FAILURE);
    }
    int flag = 0;
    fp = fopen("databases/root/users.txt", "r");
    while ((read = getline(&line, &len, fp)) != -1)
    {
        char *token = strtok(line, ":");
        if (strcmp(token, username) == 0)
        {
            token = strtok(NULL, ":");
            if (strcmp(token, password) == 0)
            {
                flag = 1;
            }
            else
            {
                break;
            }
        }
    }
    fclose(fp);
    return flag;
}

// char *sql(char q[1024], int auth){
//     char *token = strtok(q, " ");
//     // char *query[10];
//     // int i = 0;
//     if(strcmp(token, "CREATE")==0){
//         token = strtok(NULL, " ");
//         if(strcmp(token, "USER") == 0){
//             if(auth != 2){
//                 // Not allowed
//                 return "-2";
//             } else {
//                 char *username;
//                 char *password;

//                 token = strtok(NULL, " ");
//                 // printf("%s\n", token);
//                 username = token;
//                 token = strtok(NULL, " ");
//                 if(strcmp(token, "IDENTIFIED") != 0){
//                     // Wrong query
//                     return -1;
//                 }
//                 token = strtok(NULL, " ");
//                 if(strcmp(token, "BY") != 0){
//                     // Wrong query
//                     return -1;
//                 }
//                 token = strtok(NULL, " ");
//                 password = token;
//                 // printf("%s %s \n",username, password);
//                 char input_data[1024];
//                 sprintf(input_data, "\n%s:%s", username, password);
//                 FILE *fp;
//                 fp = fopen("databases/root/users.txt", "a");
//                 fputs(input_data, fp);
//                 fclose(fp);
//                 // fputs("\n", fp);
//                 // Success
//                 return 1;
//             }
//         }
//     }
//     // printf("%s", query[1]);
// }

int insert_into_users(char username[1024], char password[1024])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    if (fp == NULL)
    {
        exit(EXIT_FAILURE);
    }
    int flag = 0;
    fp = fopen("databases/root/users.txt", "r");
    while ((read = getline(&line, &len, fp)) != -1)
    {
        char *token = strtok(line, ":");
        if (strcmp(token, username) == 0)
        {
            flag = 1;
            // printf("%s %s", token, username);
        }
    }
    fclose(fp);
    if (flag == 1)
    {
        return 0;
    }
    char input_data[1024];
    sprintf(input_data, "%s:%s\n", username, password);
    fp = fopen("databases/root/users.txt", "a");
    fputs(input_data, fp);
    fclose(fp);
    return 1;
}

int add_db(char db_name[1024], char username[1024])
{
    char path[100];

    sprintf(path, "databases/%s", db_name);

    errno = 0;
    int ret = mkdir(path, S_IRWXU);
    if (ret == -1)
    {
        switch (errno)
        {
        case EEXIST:
            printf("pathname already exists");
            // Path exist
            return 0;
            break;
        default:
            perror("mkdir");
            break;
        }
    }

    char file_path[1024];
    sprintf(file_path, "%s/conf.txt", path);
    // add_user_conf(path, username);
    FILE *fp;
    fp = fopen(file_path, "w");
    fputs(username, fp);
    fputs("\n", fp);
    fclose(fp);
    return 1;
}

void add_user_conf(char path[1024], char username[1024])
{
    sprintf(path, "%s/conf.txt", path);
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    fp = fopen(path, "r");
    int flag = 0;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (strcmp(line, username) == 0)
        {
            flag = 1;
        }
    }
    fclose(fp);
    if (!flag)
    {
        fp = fopen(path, "a");
        fputs(username, fp);
        fputs("\n", fp);
        fclose(fp);
        return;
    }
    return;
}

int check_db(char db_name[1024], char username[1024])
{
    char path[100];

    sprintf(path, "databases/%s", db_name);

    DIR *dir = opendir(path);
    if (ENOENT == errno)
    {
        // Dir not exist
        return -1;
    }
    closedir(dir);
    // Check conf
    char file_path[1024];
    sprintf(file_path, "%s/conf.txt", path);
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    fp = fopen(file_path, "r");
    int flag = 0;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (strcmp(line, username) == 0)
        {
            flag = 1;
        }
    }
    if (!flag)
    {
        // Wrong username
        return -2;
    }
    // Success
    return 1;
}

int grant_user(char db_name[1024], char username[1024])
{
    char path[100];

    sprintf(path, "databases/%s", db_name);
    DIR *dir = opendir(path);
    if (ENOENT == errno)
    {
        // Dir not exist
        return 0;
    }
    closedir(dir);
    add_user_conf(path, username);
    return 1;
}
