#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <unistd.h>
#include <getopt.h>
#define PORT 8080

int main(int argc, char *argv[])
{
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char msg[1024] = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    uid_t uid = getuid(), euid = geteuid();
    int option;
    // printf("test");
    // Send if root, uid = 0
    if (uid == 0)
    {
        // sprintf(msg, "%d", uid);
        strcpy(msg, "root:root");

        send(sock, msg, strlen(msg), 0);
        // valread = read( sock , buffer, 1024);
    }
    else
    {
        char *username = NULL;
        char *password = NULL;
        while ((option = getopt(argc, argv, "u:p:")) != -1)
        {
            switch (option)
            {
            case 'u':
                username = optarg;
                // printf("username : %s\n", username);
                break;
            case 'p':
                password = optarg;
                // printf("password : %s", password);
                break;
            }
        }
        if (username == NULL && password == NULL)
        {
            printf("Credentials needed");
        }

        sprintf(msg, "%s:%s", username, password);
        send(sock, msg, strlen(msg), 0);
    }

    while (1)
    {
        valread = read(sock, buffer, 1024);
        printf("%s\n", buffer);
        if (strstr(buffer, "wrong username/password!"))
        {
            break;
        }
        memset(buffer, 0, 1024);
        char input[1024];
        scanf("%[^\n]%*c", input);

        send(sock, input, strlen(input), 0);
    }
    return 0;
}